from urllib.parse import urlparse
from bs4 import BeautifulSoup
import requests


def crawl(url, depth = 0, maxDepth = 3, visited = set()):
    if depth == 0:
        print("Crawling from " + url + "to a maximum distance of " + str(maxDepth) + " links.")
        print(url)

    depth += 1
    if depth > maxDepth:
        return

    try:
        theSet = set()
        webpage = requests.get(url)
        fullText = BeautifulSoup(webpage.text, "html.parser")


        for link in fullText.find_all("a"):
            ref = link.get("href")

            if ref[0] == "#" or len(ref) == 0 or ref is None:
                continue
            elif ref in visited:
                continue


            else:
                parse = urlparse(ref)
                if parse.scheme == '' and parse.netloc == '':
                    ref = url + ref

                theSet.add(ref)


    except:
        pass

    for current in theSet:
        spaces = ""
        for i in range(depth):
            spaces += "\t"
        print(spaces + current)
        visited.add(current)

        crawl(current, depth, maxDepth, visited)
