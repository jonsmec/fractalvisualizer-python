import sys
from urllib.parse import urlparse
import WebCrawler

def main():
    if len(sys.argv) < 2:
        print("ERROR: no URL supplied.")

    if len(sys.argv) >= 2:
        url = sys.argv[1]
        check = absCheck(url)
        if check:
            if len(sys.argv) > 2:
                maxDepth = sys.argv[2]
                WebCrawler.crawl(url=url, depth=0, maxDepth=int(maxDepth))
            else:
                WebCrawler.crawl(url)


def absCheck(url):
    parsed = urlparse(url)
    if parsed.scheme != "https" and parsed.scheme != "http":
        print("ERROR: Invalid URL supplied.")
        return False
    else:
        return True




main()